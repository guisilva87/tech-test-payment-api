using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        } 

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(int IdVendedor, List<int> IdProdutos)
        {
            Vendedor vendedor = _context.Vendedores.Find(IdVendedor);

            if (vendedor == null)
                return NotFound("Vendedor não encontrado");
            
            else if (IdProdutos.Count == 0)
            return BadRequest("A lista não pode estar vazia");

            List<Produto> produtos = new List<Produto>();

            foreach (int Id in IdProdutos)
            {
                Produto produto = _context.Produtos.Find(Id);
                if (produto == null)
                    return NotFound("Produto não encontrado");

                produtos.Add(produto);
            }

            Venda venda = new Venda(vendedor.IdVendedor, produtos);

            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("BuscarVenda{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound("Venda não encontrada");

            return Ok(venda);    
        }

        [HttpGet("ListarTodasVendas")]
        public IActionResult ListarTodasVendas()
        {
            return Ok(_context.Vendas.ToList());
        }

        [HttpPatch("AtualizarStatus")]
        public IActionResult AtualizarStatus(int Id, EnumStatusVenda status)
        {
            string opcaoEnum = status.ToString();
            var venda = _context.Vendas.Find(Id);

            if (venda == null)
            {
                return NotFound("Venda não encontrada");
            }
            else{
                switch (venda.Status){
                    case "AguardandoPagamento":
                        if (!(opcaoEnum == "PagamentoAprovado" || opcaoEnum == "Cancelada"))
                        { return BadRequest("No status de AguardandoPagamento é possível apenas retornar ou 'PagamentoAprovado' ou 'Cancelada'"); }
                        break;
                    case "PagamentoAprovado":
                        if (!(opcaoEnum == "EnviadoParaTransportadora" || opcaoEnum == "Cancelada"))
                        { return BadRequest("No status de PagamentoAprovado é possível apenas retornar ou 'EnviadoParaTransportadora' ou 'Cancelada'");}
                        break;
                    case "EnviadoParaTransportadora":
                        if (!(opcaoEnum == "Entregue" || opcaoEnum == "Cancelada"))
                        { return BadRequest("No status de EnviadoParaTransportadora é possível apenas retornar ou 'Entregue' ou 'Cancelada'");}
                        break;
                }
            }
            venda.Status = opcaoEnum;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);       
               
        }

        [HttpDelete("ExcluirVenda{Id}")]
        public IActionResult ExcluirVenda(int Id){
            var venda = _context.Vendas.Find(Id);
            if (venda == null){
                return BadRequest("Venda Não Encontrada");
            }

            _context.Vendas.Remove(venda);
            _context.SaveChanges();
            return NoContent();


    }
}

}
