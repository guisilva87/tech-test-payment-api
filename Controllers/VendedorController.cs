using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    public class VendedorController : ControllerBase
    {
       private readonly VendaContext _context;

    public VendedorController(VendaContext context)
    {
        _context = context;
    }

    [HttpPost("CadastrarVendedor")]
    public IActionResult CadastrarVendedor(string Nome, string Cpf, string Email, string Telefone)
    {
        try
        {
            Vendedor vendedorACadastrar = new Vendedor(Nome, Cpf, Email, Telefone);
            _context.Vendedores.Add(vendedorACadastrar);
            _context.SaveChanges();
            return Ok(vendedorACadastrar);
        }
        catch (ArgumentException exception)
        { 
            return BadRequest(exception.Message); 
        }

    }
    
    [HttpGet("BuscarPor{Id}")]
    public IActionResult BuscarVendedor(int Id){
        var vendedor = _context.Vendedores.Find(Id);
        if (vendedor == null){
            return NotFound();
        }

        return Ok(vendedor);
    }

    [HttpGet("ListarVendedores")]
    public IActionResult BuscarTodosVendedores(){
        var vendedores = _context.Vendedores.ToList();
        return Ok(vendedores);
    }

    [HttpDelete("ExcluirVendedor{Id}")]
    public IActionResult ExlcuirVendedor(int Id){
        var vendedor = _context.Vendedores.Find(Id);
        if (vendedor == null){
            return NotFound();
        }

        _context.Vendedores.Remove(vendedor);
        _context.SaveChanges();
        return NoContent();
    }
 
    }
}