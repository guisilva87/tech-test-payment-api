using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
       private readonly VendaContext _context;

        public ProdutoController(VendaContext context)
        {
            _context = context;
        }
    
        [HttpPost("CadastrarProduto")]
        public IActionResult CadastrarProduto(string Nome)
        {
            Produto produto = new Produto(Nome);

            _context.Produtos.Add(produto);
            _context.SaveChanges();
            return Ok(produto);
        }

        [HttpGet("BuscarProduto{Id}")]
        public IActionResult BuscarProduto(int Id)
        {
            var produto = _context.Produtos.Find(Id);
            if (produto == null){
                return NotFound("Produto não Encontrado");
            }
            return Ok(produto);
        }

        [HttpGet("ListarTodosOsProdutos")]
        public IActionResult ListarTodosOsProdutos(){
            return Ok(_context.Produtos.ToList());
        }

        [HttpPut("AtualizarProduto")]
        public IActionResult Produto(int Id, string Nome){
            var produto = _context.Produtos.Find(Id);
            if (produto == null){
                return NotFound("Produto não Encontrado");
            }
            produto.Nome = Nome;
            _context.Produtos.Update(produto);
            _context.SaveChanges();
            return Ok(produto);
        }

        [HttpDelete("ExcluirProduto{Id}")]
        public IActionResult ExcluirProduto(int Id){
            var produto = _context.Produtos.Find(Id);
            if (produto == null){
                return NotFound("Produto não Encontrado");
            }

            _context.Produtos.Remove(produto);
            _context.SaveChanges();
            return NoContent();
        }
 
    }
}