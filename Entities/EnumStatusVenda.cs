using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        PagamentoAprovado,
        Cancelada,
        EnviadoParaTransportadora,
        Entregue
    }
}