using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Produto
    {
        public Produto(string Nome)
        {
            this.Nome = Nome;
        }

        public int IdProduto { get; set; }
        public string Nome { get; set; }

    }
}